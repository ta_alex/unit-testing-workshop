# Unit Testing Workshop

## Clone Repo
Navigate to the folder of your choice and clone this repository using HTTPS
```bash
git clone https://git.rwth-aachen.de/fst-tuda/projects/rdm/unit-testing-workshop.git
cd unit-testing-workshop
```

or using SSH: 

```bash
git clone git@git.rwth-aachen.de:fst-tuda/projects/rdm/unit-testing-workshop.git
cd unit-testing-workshop
```

## Setting up virtual environment (venv)
In order to use the same version of python and other packages, we will set up a virtual environment using `pip` (an open source, non-proprietory alternative to Anaconda).

### Create the virtual environment called `venv`

Windows:
```bash
py -m venv venv
```
(By problems try `C:\> py -m venv venv`)

Mac:
```bash
python3 -m venv venv
```

### Activate the virtual environment venv

Windows:
```bash
venv\Scripts\activate
```
(By problems try `C:\> venv\Scripts\activate`)

Mac:
```bash
source venv/bin/activate
```
### Upgrade packages in venv
```bash
pip install -U pip setuptools
```

If you get an error that looks like this:
```
ERROR: Could not install packages due to an OSError: [WinError 5] Zugriff verweigert: 'C:\\Users\\Lestakova\\AppData\\Local\\Temp\\pip-uninstall-f8cwia31\\pip.exe'mp\\pip-uninstall-f8cwia31\\pip.exe'
Check the permissions.
```
ignore it.
### Install dependencies from requirements.txt

```bash
pip install -r requirements.txt
```

With this step, the setup is complete.


## Running tests
```bash
pytest
```

## Generate test coverage
```bash
pytest --cov=math_operations tests/ --cov-report=html --cov-branch
```
