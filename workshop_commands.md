## Setting up virtual environment (venv)

### create venv

Windows:
```console
C:\> py -m venv venv
```
Mac:
```console
$ python3 -m venv venv
```

### activate venv

Windows:
```console
C:\> venv\Scripts\activate
```
Mac:
```console
$ source venv/bin/activate
```
### upgrade packages
```console
$ pip install -U pip setuptools
```

### Install requirements

```console
$ pip install -r requirements.txt
```

## Running tests
```console
$ pytest
```

## Generate test coverage
```console
$ pytest --cov=math_operations tests/ --cov-report=html --cov-branch
```