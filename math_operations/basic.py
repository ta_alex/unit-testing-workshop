from typing import Union

def add(num1: Union[int, float], num2: Union[int, float]) -> Union[int, float]:
    return num1 + num2
